(**
 * Läs argument och växlar från kommandoraden.
 *
 * https://ocaml.org/learn/tutorials/command-line_arguments.html
 * http://caml.inria.fr/pub/docs/manual-ocaml/libref/Arg.html
 *
 * Här använder vi modulen Arg från standardbiblioteket.
 * 
 * Progammet specificerar några växlar, och tar och sedan 
 * tolkar och sätter några variabler utifrån vad som kommer på
 * kommandoraden. 
 *
 * $ ./throwdice.byte -side6 5
 * $ -/throwdice.byte --help
 *
 *)

(* Skapa variablerna som sätts enligt kommandoraden *)
let side4a = ref int
let restofargs = ref ""

(* Tolka kommandoraden *)
let () =
  let spec = [("-side4", Arg.Int (fun n -> side4a := n), "\t Kasta n 4-sidiga");
              ("--", Arg.Rest (fun s -> restofargs := !restofargs ^ s ^ " "),
               "\t alla övriga argument");]
  in
  Arg.parse
    (Arg.align spec)
    (fun str -> restofargs := ! restofargs ^ str ^ " ")
    "Implementerar övning2.\n"

let skapa1 a b c =
  []

let () =
  let generate a = a in
  let print_result a = a in
  in
  (* Ok, så nu skriver vi ut argumenten. *)
  return !skapa4a |> bind generate |> print_result
(* eof *)
    
    
