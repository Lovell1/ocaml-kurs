(** Modul Die, fil: die.ml Funktorn [Die.Make] tar en modul av typen
   [module type DIETYPE] och skapar en ny modul av typen [module type
   D], som används för att hantera en tärning av viss antal sidor. *)

(** Anger vilka funktioner och datatyper som en tärning har *)
module type D =
sig
  (** abstrakta datatypen [die] *)
  type die
  (** [new_die] är en tom ny tärning *) 
  val new_die  : die
  (** [sides] anger antal sidor i som tärningarna har *)
  val sides    : int
  (** [eval d] ger tärningens värde i intervallet 1 .. sides. Om
     tärningen inte är slagen, så kommer den att generera nya
     slumpmässiga värden *)
  val eval     : die -> int
  (** [throws d] kommer att generera en ny slagen tärning *)
  val throws   : die -> die
end

(** Den modul som anger antalet sidor, som [Die.Make] använder sig av *)
module type DIETYPE =
sig
  val sides : int
end

(** [Make d] skapar en ny modul av typen [Die.DIETYPE] som hanterar
   tärningar med [d.sides] sidor *)
module Make(Die : DIETYPE) : D =
struct
  type die = (int*int) option
  let sides = Die.sides
  let new_die = None
  let newthrow () = (Random.int sides) + 1
  let eval d=
    match d with
    | Some (value, _) -> value
    | None -> newthrow ()
  let throws d =
    match d with
    | None -> Some (newthrow () , sides)
    | Some (_, sides) -> Some (newthrow (), sides)
end
(* Die ends here *)
