(** Modulen [Status] som hanterar status genom alla stegen i Railway
   Pattern. *)

type data =
  {
    argument : string;
    filnamn  : string option
  }

type status =
  (data,string) result

let empty = Result.ok { argument=""; filnamn=Option.none }

let set_filnamn filnamn data =
  let fn = Option.some filnamn in
  Result.ok { data  with filnamn = fn }

let get_filnamn { filnamn=fn ; _ } =
  fn

let set_argument arg data =
  Result.ok { data with argument = arg }

let get_argument { argument = arg; _ } =
  arg

(* eof *)

