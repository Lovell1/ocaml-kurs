(** Modulen [Status] som hanterar status genom alla stegen i Railway
   Pattern.*)

type data
(** [data] lagrar all data *)

type status = (data, string) result
(** [status] innehåller mellanresultatet mellan alla stegen, [data]
   eller [string felmedelande] *) 


(** [empty] är tomma status med default-värden *)
val empty : status

(** [set_filnamn filnamn data] skapar nytt [status] från [data] ned
   nytt värde på [filnamn] *)
val set_filnamn : string -> data -> status

(** [get_filnamn data filnamn] ger filnamnet från [data] *)
val get_filnamn : data -> string option


(** [set_argument arg] sätter [arg] i nya [status] *)
val set_argument : string -> data -> status

(** [get_argument data]  ger den sträng som finns i [data] *)
val get_argument : data -> string

(* eof *)
