(** [databas] hanterar läsning och skrivning till [sqlite3].

    Vi använder oss av [sqlite3] och hanterar [movie_record] för att
    hantera in och ut-matning *)


type movie_record = {
    title : string;
    cast : string list;
    year : int;
  } [@@deriving yojson {strict = false}]
(* skapar to_yojson on from_yojson *)


module CleanName = struct
  let r1 = Str.regexp {|^\[\[\(.*\)\]\]|}
  let r2 = Str.regexp {|\(|.*\)$|}
  let r3 = Str.regexp {| *(.*actor) *$|}
  let r4 = Str.regexp {|'|}

  let clean_name s =
    Str.replace_first r1 "\\1" s |>
      Str.replace_first r2 "" |>
      Str.replace_first r3 ""
end

let clean_name = CleanName.clean_name

module BuildDb = struct
  let create_db () =
    let open Sqlite3 in
    let mydb = Sqlite3.db_open "kbnum.db" in
    let sql =
      {|
       DROP TABLE IF EXISTS actors;
       CREATE TABLE actors (
          actor_id INTEGER PRIMARY KEY,
          actor_name TEXT NOT NULL
       );
       CREATE UNIQUE INDEX actror_name_idx ON actors (actor_name);
       DROP TABLE IF EXISTS movies;
       CREATE TABLE movies (
          movie_id INTEGER PRIMARY KEY,
          movie_name TEXT NOT NULL
       );
       CREATE UNIQUE INDEX movie_name_idx ON movies (movie_name);
       DROP TABLE IF EXISTS parts;
       CREATE TABLE parts (
          movie_id INTEGER NOT NULL,
          actor_id INTEGER NOT NULL,
          PRIMARY KEY (movie_id, actor_id)
       );
       CREATE INDEX parts_actor_idx ON parts (actor_id);
       |}
    in
    begin
      match exec mydb sql with
      | Rc.OK -> print_endline "Ok"
      | rc -> prerr_endline (Rc.to_string rc); prerr_endline (errmsg mydb);
    end;
    mydb
  let mydb = create_db ()

  let exe sql =
    let open Sqlite3 in
    match exec mydb sql with
    | Rc.OK -> ()
    | rc -> prerr_endline (Rc.to_string rc); prerr_endline (errmsg mydb)

  let begin_transaction = exe "BEGIN TRANSACTION; "
  let end_transaction = exe "END TRANSACTION; "

  type id_tbl = {
      tbl   : (string, int) Hashtbl.t;
      count : int ref;
      stmt  : Sqlite3.stmt
    }

  let insert_actor = Sqlite3.prepare mydb "INSERT INTO actors VALUES(?,?);"
  let insert_movie = Sqlite3.prepare mydb "INSERT INTO movies VALUES(?,?);"
  let insert_parts = Sqlite3.prepare mydb "INSERT INTO parts  VALUES(?,?);"

  let create_tbl stmt = {
      tbl = Hashtbl.create 10000;
      count = ref 10000;
      stmt = stmt;
    }

  let actors = create_tbl insert_actor
  let movies = create_tbl insert_movie

  let insert_is stmt i str =
    let open Sqlite3 in
    ignore @@ reset stmt;
    ignore @@ bind_int stmt 1 i;
    ignore @@ bind_text stmt 2 str;
    match step stmt with
    | Rc.OK | Rc.DONE -> Some (i, str)
    | rc -> prerr_endline (Rc.to_string rc); prerr_endline (errmsg mydb);
            None

  let insert_ii stmt i1 i2 =
    let open Sqlite3 in
    ignore @@ reset stmt;
    ignore @@ bind_int stmt 1 i1;
    ignore @@ bind_int stmt 2 i2;
    match step stmt with
    | Rc.OK | Rc.DONE -> Some (i1, i2)
    | rc -> prerr_endline (Rc.to_string rc) ; prerr_endline (errmsg mydb);
            None

  let get { tbl; count; stmt } name =
    match Hashtbl.find_opt tbl name with
    | Some id -> Some id
    | None ->
       incr count;
       Hashtbl.add tbl name (!count);
       match insert_is stmt (!count) name with
       | None -> None
       | Some (id, _name) -> Some id

  let tabulate title cast =
    match get movies (clean_name title) with
    | None -> 0
    | Some movie_id ->
       let rec aux acc = function
         | [] -> acc
         | name :: cast ->
            begin
              match get actors name with
              | None -> aux acc cast
              | Some actor_id ->
                 match insert_ii insert_parts movie_id actor_id with
                 | Some _ -> aux (acc + 1) cast
                 | None ->
                    prerr_endline ("Movie: " ^ title ^ " Actor: " ^ name);
                    aux acc cast
            end
       in
       aux 0 (List.map clean_name cast)

  let read_file_into_db filename =
    begin_transaction;
    let chan = open_in filename in
    let count = ref 0 in
    try
      while true do
        let line = input_line chan in
        match Yojson.Safe.from_string line |>
                movie_record_of_yojson with
        | Result.Ok { title ; cast; _ } ->
           count := !count + (tabulate title cast)
        | Result.Error _ -> ()
      done;
      !count
    with End_of_file ->
      close_in chan; end_transaction;
      ignore @@ Sqlite3.db_close mydb;
      !count
                        
end
(*
module ReadDb = struct
  type id =
    | String of string
    | Id of int
    | Empty

  type vertex =
    | Actor of id
    | Movie of id

  let data_to_id = function
    | Sqlite3.Data.INT i64 -> Id (Int64.to_int i64)
    | Sqlite3.Data.TEXT s -> String s
    | _ -> String "error in data_to_id"

  let actor x = Actor (Id x)
  let movie x = Movie (Id x)

  let mydb = Sqlite3.db_open "kbnum.db"

  let get_actor   = Sqlite3.prepare mydb "SELECT actor_name FROM actors WHERE actor_id = ?;"
  let get_movie   = Sqlite3.prepare mydb "SELECT movie_name FROM movies WHERE movie_id = ?;"
  let get_actor_i = Sqlite3.prepare mydb "SELECT actor_id   FROM actors WHERE actor_name = ?;"
  let get_movie_i = Sqlite3.prepare mydb "SELECT movie_id   FROM movies WHERE movie_name = ?;"
  let get_actor_p = Sqlite3.prepare mydb "SELECT actor_id   FROM parts  WHERE movie_id = ?;"
  let get_movie_p = Sqlite3.prepare mydb "SELECT movie_id   FROM parts  WHERE actor_id = ?;"

  let bind stmt data =
    let open Sqlite3 in
    ignore @@ reset stmt;
    bind stmt data (Data.INT data)

  let get_id_p v =
    let open Sqlite3 in
    let stmt =
      match v with
      | Actor (String _s) -> get_actor_i
      | Movie (String _s) -> get_movie_i
      | _ -> failwith "get_it v: Wrong arg"
    in
    let ff acc row =
      match row.(0) with
      | Data.TEXT s ->
         begin
           match v with
           | Actor _ -> Actor (String s)
           | Movie _ -> Movie (String s)
         end
      | Data.INT id ->
         begin
           match v with
           | Actor _ -> Actor (Id id)
           | Movie _ -> Movie (Id id)
         end
      | _ -> acc
    in
    ignore @@ reset stmt;
    ignore @@ bind_text stmt 1 s;
    match step stmt with
    | Rc.ROW -> Some (ff row_data stmt)
    | _ -> None

  let get_id_i n =
    let open Sqlite3 in
    let (s,stmt) =
      match n with
      | Actor_name x -> x, get_actor_i
      | Movie_name x -> x, get_movie_i
    in
    ignore @@ reset stmt;
    ignore @@ bind_text stmt 1 s;
    match step stmt with
    | Rc.ROW ->
       begin
         match (row_data stmt).(0) with
         | Data.INT id ->
            let i = (Int64.to_int id) in
            match n with
            | Actor_nameg _ -> Some(Actor i)
            | Movie_name _ -> Some(Movie i)
            | _ -> None
       end
    | _rc -> None

  let fold_parts f v acc =
    let open Sqlite3 in
    let (x, stmt) =
      match v with
      | Actor x -> x, get_movie_p
      | Movie x -> x, get_actor_p
    in
    let ff acc row =
      match row.(0) with
      | Data.INT id ->
         let i = Int64.to_int id in
         f (match v with
            | Actor _ -> Movie i
            | Movie _ -> Actor i)
           acc
      | _ -> acc
    in
    ignore @@ reset stmt;
    ignore @@ bind_int stmt 1 x;
    let (rc, acc') = fold stmt ~f:ff ~init:acc in
    match rc with
    | Rc.OK | Rc.DONE -> acc'
    | rc -> prerr_endline (Rc.to_string rc); prerr_endline (errmsg mudb);
            acc'
    
end

 *)
