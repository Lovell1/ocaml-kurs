#+title: Skapa projekt i OCaml
#+subtitle: Skapa projek och program med =dune=
#+author: Anders Jackson
#+email: anders.jackson@hig.se
#+language: sv
#+option: toc:nil
#+option: num:nil
#+startup: showall
#+property: header-args :comments both
#+property: header-args :eval never-export
#+property: header+args:sh :exports both :results output
#+property: header+args:sh :dir ./Projekt_2021 :mkdirp t

* För att bygga projekt i OCaml

För att skapa program i OCaml, så kan man med fördel använda
programmet =dune=.  Det hanterar allt som använda programbibliotek som
laddas ned med exempelvis =opam=, kompilera program med =ocamlbuild=
och exekvera programment.

Programmet =dune= använder två nivåer, =dev= och =release=.  Valet om
man inte anger något är =dev=, men kan med växeln =-p= sättas till
=release=.  Skillnaden är att =dev= har mer felsökning, medans
=release= är optimerad för exekvering och storlek.

Här behöver vi inte bry oss om det.

* Förutsättningar

Man har en standardinstallation och installerar paket med =opam= i den.

Alternativt, avancerat, så kan skapa en temporär installation av Ocaml
för projektet. Vi skall installera alla bibliotek etc i den.  Det
behövs bara göras en gång.  Det behövs inte här.

** Använda befintlig installation

Vi behöver installera beroenden med =opam install=.
Programbiblioteket =sqlite3= behöver dessutom installera
utvecklingsmiljön för Sqlite 3, vilket görs med =apt install
sqlite3-dev= i Ubuntu. I Mac OSX så behöver man föra =brew install
pkg-config= innan man installera paketen med =opam=.

#+caption: Installera beroenden
#+BEGIN_SRC shell
  opam install yojson ppx_deriving_yojson ocamlgraph sqlite3
#+END_SRC

** Ny temporär installation

Behövs inte här, men kan vara bra om man vill ha flera olika
installationer till olika kunder/projekt.

#+caption: Skapa en installation av Ocaml
#+name: shell:opam_switch_create
#+BEGIN_SRC shell :dir
  LC_ALL=C opam switch --yes --description="Till projekt" create proj 4.12.0
#+END_SRC

Varje gång vi vill använda projektet, så skall vi köra följande, så
att program och bibliotek/moduler hittas.

#+caption: Aktiverar test-projektet
#+name: shell:opam_switch_set
#+BEGIN_SRC shell :dir
  opam switch set proj
  eval $(opam env)
#+END_SRC

Så nu installerar vi paketen som behöver för utveckling och för
projektet med =opam=.

#+caption: Installera nödvändiga paket i switch =proj=
#+name: shell:opam_install
#+BEGIN_SRC shell :dir
  opam install --yes dune yojson ocamlgraph
  opam install --yes utop merlin ocaml-lsp-server ocamlformat ocp-indent ounit2
#+END_SRC

När vi är färdiga med denna switch, vi kan ta bort den med följande
kommando

#+caption: 
#+name: shell:opam_switch_remove
#+BEGIN_SRC shell :dir
  opam switch remove proj
#+END_SRC 

* Projektet

Har beskriver vi hur man kan använda =dune=.

** Skapa projektet

Skapa och gå till ~Code/Projekt~.

#+caption: Gå till rätt katalog för att skapa projektet
#+name: shell:create_project_dir
#+BEGIN_SRC shell :eval never
   mkdir -p Code/Projekt/Projekt_2021
   cd Code/Projekt/Projekt_2021
#+END_SRC

Skapa projektet med =dune init= och ange vad man vill skapa, i detta
fallet ett project, men man kan även skapa =exec= och =lib=. Vi anger
projektets namn och vilka programbibliotek vi vill använda.  Här
behöver vi =yojson= och =ocamlgraph= för att läsa =Json= och hantera
grafer.

#+caption: Skapa projektet med rätt programbibliotek
#+name: shell:dune_init_project
#+BEGIN_SRC shell
   dune init proj kbnum --libs=yojson,ocamlgraph,sqlite3 --ppx ppx_deriving_yojson
#+END_SRC

#+RESULTS: shell:dune_init_project

Programmet finns nu under ~./kbnum/bin/main.ml~ och den använder moduler
under ~./kbnum/lib/~. Tester kan läggas under ~./kbnum/test/~.  När
det installeras, så får det namnet ~kbnum~.

Nu går man lämpligen ned i katalogen ~.kbnum/~ för att enklare
redigera filer och bygga programmet.

Detta projekt kan skapa genom att köra =dune exec kbnum= i katalogen
som skapas.  Men det är alltså i filen ~./bin/main.ml~ som programmet
skrivs.

*** Flera körbara program.

Behöver vi inte här, men kan vara bra att veta.

För att installera ett exekverbart program, kan man använd kommandon i
listning [[shell:dune_init_exec]].  Då kommer det skapa en katalog
~./bin/kbgraf.ml~ samt övriga filer som behövs. Notera att ~bin~ efter
=kbgraf= är katalogen som programmet =kbgraf= kommer att installeras.

#+caption: Skapa programmet i projektet
#+name: shell:dune_init_exec
#+BEGIN_SRC shell
   dune init exe kbgraf bin --libs=yojson,ocamlgraph,sqlite3 --ppx ppx_deriving_yojson
#+END_SRC

#+RESULTS: shell:dune_init_exec

Källkoden finns då som =./bin/kbgraf.ml=

*** Flera moduler/bibliotek.

Klarar vi oss även utan, men är bra att känna till.

Övriga programfiler lagras som ett bibliotek. Då skapas det en katalog
~./libs/data.ml~ för modulen =Data=.

#+caption: Skapa programbibliotek i projektet under ~libs~
#+name: shell:dune_init_lib
#+BEGIN_SRC shell
   dune init lib data libs
#+END_SRC
 
Om man vill att huvudprogrammet ~main.ml~ skall använda det, så
behöver man lägga till det i ~./bin/dune~ som ett bibliotek =data=.

** Kompilera

Nu kan man redigera och skapa filer i katalogen ~_build~.  För att
kompilera programmet och sedan exekvera, så kan man köra
dune-kommandot build.  Om man vill se alla stegen som =dune= gör, så
lägg till växeln =--verbose=.

#+caption: Kompilera, bygg, projektet
#+name: shell:dune_build
#+BEGIN_SRC shell 
  dune build --verbose
#+END_SRC

#+RESULTS: shell:dune_build

Man kan starta kompileringen så att =dune= hela tiden kontrollerar
filer som ändras, och då startar en kompilering vid behov.  Notera att
man kan behöva installera =inotifywait= för att det skall fungera.
Det gör man med =sudo apt install inotify-tool= i
Debian-distributioner som Ubuntu samt på Apple OSX-datorer fungerar
=brew install fswatch=.

Körs med fördel i ett eget terminalfönster.

#+caption: Kompilera om när filer spara.
#+name: shell:dune_buiild_watch
#+BEGIN_SRC shell
  dune build --watch
#+END_SRC

** Exekvera

För att exekvera programmet, så kan man köra programmet i
~_build~-katalogen.  Eller så använder man dune-kommandot =exec
./bin/main.exe=

#+caption: Exekvera programmet =main.exe= och =kbgraf.exe=
#+name: shell:dune_exe
#+BEGIN_SRC shell
  dune exec ./bin/main.exe
  dune exec ./kbgraf.exe
#+END_SRC

*** Exekvera i =utop=

För att testa, så kan man använda =dune=-kommandot =utop=, för att
kunna testa interaktiv.

#+caption: Kör progammet med =utop=
#+name: shell:dune_top
#+BEGIN_SRC shell
  dune utop lib # modulerna i lib
  dune utop
#+END_SRC 

** Rensa

För att rensa bort onödiga filer, så kan man använda =dune=-kommandot
=dune clean=.  Det tar bort filer som kan återskapas med =dune=. Tar
bort de filer som inte behöver läggas under versionshantering.

#+caption: Radera filer som kan återskapas
#+name: shell:dune_clean
#+BEGIN_SRC shell
  dune clean
#+END_SRC

** Hjälp

Man kan få hjälp hur =dune= fungerar genom att använda växeln =--help=.
Exempelvis för att se alla  =dune=-kommandon, så gör man bara

#+caption: Hjälp om =dune=-kommandon
#+name: shell:dune_help
#+BEGIN_SRC shell
  dune --help
#+END_SRC

För att se hur =dune=-kommandot =init= fungerar, så kan man ge
=--help= efter =init=, så här

#+caption: Hjälp om =dune init=
#+name: shell:dune_init_help
#+BEGIN_SRC shell :dir Projekt_2021
  dune init --help
#+END_SRC

** Använda =make=

Om man vill använda en =Makefile= med =dune=, så kan den se ut som
följer.

#+BEGIN_SRC makefile 
  .PHONEY: all run clean doc
  all:
	  dune build
  run:
	  dune exec ./bin/main.exe
# eller   dune exec kbnum
  clean:
	  dune clean
  doc:
	  dune build @doc
#+END_SRC

* Referenser

- [[https://dune.build/examples][Dune officiella hemsida m. exempel]] :: Här hittar ni mer om =dune=.
- [[https://github.com/ocaml/dune/blob/main/doc/usage.rst][Command-line interface - GitHub : ocaml/dune]] :: Från =ocaml@GitHub=
  om hur =dune= fungerar på kommandoraden. Mera dokumentation om
  =dune= hittar man i [[https://github.com/ocaml/dune/tree/main/doc][=main/doc/=-katalogen]] på samma ställe.  Här kan
  man hitta mycket, exempelvis om testning.
- [[https://github.com/lindig/hello][Hello - GitHub : lindig/hello]] :: Ett enkelt =dune=-projekt som man
  kan utgå från när man skriver kod för Ocaml.
- [[https://medium.com/@bobbypriambodo/starting-an-ocaml-app-project-using-dune-d4f74e291de8][Start an OCaml app project using Dune]] :: En minimal användning av
  =dune= för att skapa ett projekt.
- [[https://discuss.ocaml.org/tag/dune][OCaml forum om dune]] :: Här kan man hitta avancerade, och inte fullt
  så avancerade egenskaper om =dune=.
- [[https://github.com/lindig/ocaml-style][OCaml Towards Clarity and Grace]] :: En stilguide för att hjälpa att
  skriva tydlig kod i OCaml.

# Local Variables:
# eval: (require 'ob-shell)
# eval: (require 'ob-ocaml)
# End:
