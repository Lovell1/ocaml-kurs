#! /usr/bin/env ocaml
;;
(** my_test_set.ml 

    Här beskriver vi hur modulen Set kan användas.
   
    Dels bygger vi ett Set baserat på modulen String, som innehåller
    rätt signatur. Dvs sig type t val compare: t -> t -> int end
    Det är som Comparator i Java.

    Dels bygger vi den egna datastrukturen kakor, och sedan 
    en modul som har rätt signatur för att Set.Make skall
    kunna bygga en Set-modul utifrån den. 

    Den här behöver läsas in i utop, så C-c C-b i Emacs *)

(* Läs in i utop #use eller #mod_use,
  #mod_use "my_test_set.ml";;
  Lägger namnen i modulen My_test_set
 *)

(* Titta på sig på Set, Set.Make och String, se att String har type t
   och val compare: 'a -> 'a -> int, som Set.OrderedType vilket
   betyder att String implementerar allt som Set.OrderedType behöver. *)
#show Set;;
#show Set.Make;;

#show String;;

module StringSet = Set.Make(String)
;;
(* Se signatuen, sig, på den genererade modulen StingSet *)

#show StringSet;;

let set1 = StringSet.(empty |> add "a" |> add "b") in
print_newline ();
Printf.printf " min_elt set1  = %s\n" (StringSet.min_elt set1);
Printf.printf " cardinal set1 = %d\n" (StringSet.cardinal set1)
;;

(** Ny egen datastruktur, vi kan bygga större själva *)
type kakor =
    Bulle
  | Limpa
  | Finska_pinnar
;;    
(** string_of_kakor : kakor -> string *)
let string_of_kakor = function
    Bulle -> "bulle"
  | Limpa -> "limpa"
  | Finska_pinnar -> "finska pinnar"
;;

(** Skapa en modul Kakor som hanterar [kakor] ([type t = kakor]) och
   jämför dem ([val compare: t -> t -> int]) *)
module Kakor = struct
  type t = kakor
  let compare = Stdlib.compare
end
(** Skapa modulen KakorSet från funktorn (modulfunktionen) Set.Make *)
#show Kakor
;;                 
#show KakorSet
;;
module KakorSet = Set.Make(Kakor)
;;

(* Provkör lite exempel, fler funktioner finns i ocaml-lang.pdf *)
let kakor_set = KakorSet.(empty |> add Bulle |> add Limpa) in
print_newline ();
Printf.printf " min_elt  kakor_set = %s\n"
  (string_of_kakor (KakorSet.min_elt kakor_set));
Printf.printf " cardinal kakor_set = %d\n"
  (KakorSet.cardinal kakor_set)
;;

