(*
 * Läs argument och växlar från kommandoraden.
 *
 * https://ocaml.org/learn/tutorials/command-line_arguments.html
 * http://caml.inria.fr/pub/docs/manual-ocaml/libref/Arg.html
 *
 * Här använder vi modulen Arg från standardbiblioteket.
 * 
 * Progammet specificerar några växlar, och tar och sedan 
 * tolkar och sätter några variabler utifrån vad som kommer på
 * kommandoraden. 
 *
 * ocamlbuild -cflags -annot ovning1.byte
 * 
 * $ ./ovning1.byte skapa1
 * $ -/ovning1.byte --help
 *
 *)

(* Skapa variablerna som sätts enligt kommandoraden *)
let skapa1a = ref false
let skapa2a = ref false
let skapa3a = ref false
let oka1a = ref false
let oka2a = ref false
let oka3a = ref false
let restofargs = ref ""

(** Tolka kommandoraden först i programmet *)
let () =
  let spec = [("-skapa1",  Arg.Set skapa1a, "\t testar skapa1");
              ("-skapa2",  Arg.Set skapa2a, "\t testar skapa2");
              ("-skapa3",  Arg.Set skapa3a, "\t testar skapa3");
              ("-okamed1", Arg.Set oka1a,   "\t testar okamed1");
              ("-okamed2", Arg.Set oka2a,   "\t testar okamed2");
              ("-okamed3", Arg.Set oka3a,   "\t testar okamed3");
              ("--", Arg.Rest (fun s -> restofargs := !restofargs ^ s ^ " "),
               "\t alla övriga argument");]
  in
  Arg.parse
    (Arg.align spec)
    (fun str -> restofargs := ! restofargs ^ str ^ " ")
    "Implementerar övning1.\n"

(** [skapa1 size start step] ger en lista med [size] element, startar
   med [start] och har [step] steg mellan elementen.
 [let val skapa1: int -> int -> int -> int list]*)
let rec skapa1 size start step =
  if size = 0 then
    []
  else
    start :: (skapa1 (size - 1) (start + step) step)

(** [skapa2 size start step] ger en lista med [size] element, startar
   med [start] och har [step] steg mellan elementen.
 [let val skapa2: int -> float -> float -> float list]*)
let rec skapa2 size start step =
  match size with
  | 0 -> []
  | v -> start :: (skapa2 (size - 1) (start +. step) step)

(** [skapa3 size start nextf] ger en lista med [size] element, startar
   med [start] och använder funktionen [nextf] för att ge
   efterföljande element.
 [let val skapa3: int -> 'a -> ('a -> 'a) -> 'a list]*)
let rec skapa3 size start nextf =
  match size with
  | 0 -> []
  | v -> start :: (skapa3 (size - 1) (nextf start) nextf)

(** Ökar alla element i [int list] med ett
 [val okamed1: int list -> int list] *)
let okamed1 lst =
  List.map ((+) 1) lst

(** Öka alla element i [float list] med [okning]
 [val okamed2: float list -> float] *)
let rec okamed2 lst stp =
  match lst with
  | [] -> []
  | v :: rest -> (v +. stp) :: okamed2 rest stp

(** Gå igenom listan ['a list] och använd funktionen ['a -> 'a] på
   alla element 
 [val okamed3: 'a list -> ('a -> 'a) -> 'a list] *)
let okamed3 lst nextf =
  List.map nextf lst

(** Kör programmet efter att vi tolkat kommandoraden *)
let () =
  (** [val string_list_to_string: string list -> string *)
  let string_list_to_string list =
    let str = String.concat "; " list in
    "[ " ^ str ^ " ]"
  in
  (** [val skrivut: string -> fmt_string -> 'a list -> unit] *)
  let skrivut pre fmt lista =
    (** [to_string_list] Konvertera från ['a list] till [string list], med
       formatsträngen [fmt] *)
    let to_string_list = List.map (Printf.sprintf fmt) lista in
    Printf.printf "%s -> %s\n" pre (string_list_to_string to_string_list)
  in
  (* Ok, så nu skriver vi ut argumenten. *)
  if !skapa1a then
    begin
      skrivut "skapa1 4 10 2"  "%d" (skapa1 4 10 2)
      (* skrivut "skapa1 0 10 2"  "%d" (skapa1 0 10 2);
       * skrivut "skapa1 4 50 -5" "%d" (skapa1 4 50 (-5))
       *)
    end
  else if ! skapa2a then
    begin
      skrivut "skapa2 4 10.0 2.0"  "%F" (skapa2 4 10. 2.)
    end
  else if ! skapa3a then
    begin
      skrivut "skapa3 4 4.1 (fun tal -> tal +. 0.2)" "%F"
        (skapa3 4 4.1 (fun tal -> tal +. 0.2))
    end
  else if ! oka1a then
    begin
      skrivut "okamed1 [ 2; 4; 6 ]" "%d"
        (okamed1 [ 2; 4; 6 ])
    end
  else if ! oka2a then
    begin
      skrivut "okamed2 [ 2.; 4.; 5. ] 2.0" "%F"
        (okamed2 [ 2.; 4.; 5. ] 2.0)
    end
  else if ! oka3a then
    begin
      skrivut "okamed3 [ 2.; 4.2; 5. ] (fun e -> e +. 2.1)" "%F"
        (okamed3 [ 2.; 4.2; 5. ] (fun e -> e +. 2.1))
    end
  else
    print_endline "not -skapa1"

  (* Printf.(printf "skapa1:            %b\n" !skapa1a;
   *         printf "skapa2:            %b\n" !skapa2a;
   *         printf "skapa3:            %b\n" !skapa3a;
   *         printf "Rest of line: %s\n" !restofargs)
   *)

(* eof *)
    
    
