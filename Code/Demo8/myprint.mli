(* myprint.mli *)
(** [Myprint.output_result] print the result of calculation according
   to [Myargs.state] we got from [Myargs.parse]
*)

(** [output_result s] takes the [Calc.state] and print a result *)
val output_result: Calc.state -> unit

(* eof *)
