(* myprint.ml *)

let output_result state =
  if Calc.is_error state then
    prerr_string (Calc.get_error state)
  else
    if Calc.is_addition state then begin
      Printf.printf "Add: %f\n " (Calc.get_value state);
    end
    else begin
      Printf.printf "Sub: %f\n" (Calc.get_value state);
    end;
    if Calc.is_verbose state then begin
      List.iter (Printf.printf "%f ") (Calc.get_arguments state);
      print_newline ()
    end

(* eof *)
