(* calc.ml *)

(** [Calc.calculate s] kontrollerar [Mystate.state] om det är ok, och i
   så fall gör önskad beräkning. Om det skett ett fel tidigare, så
   skickas felmeddelandet vidare *)

type my_state = Myargs.state * float
type state = (my_state, string) result
let my_default_state = Myargs.default_state, 0.0
let default_state:state = Result.ok my_default_state

let is_error = Result.is_error
let is_value = Result.is_ok
                 
let get_error = Result.get_error

let get_value s = snd (Result.value ~default:my_default_state s)
let get_state s = fst (Result.value ~default:my_default_state s)
  
let is_addition s = Myargs.is_addition (get_state s) 
let is_subtraction s = Myargs.is_subtraction (get_state s)
let is_verbose s = Myargs.is_verbose (get_state s)
    
let num_arguments s = Myargs.num_arguments (get_state s)
let get_arguments s = Myargs.get_arguments (get_state s)

let calc state =
  let arglist = Myargs.get_arguments state in
  if (List.length arglist) = 0 then
    0.0
  else
    let first = List.hd arglist in
    let rest = List.tl arglist in
    if Myargs.is_addition state then
      List.fold_left (+.) first rest
    else
      List.fold_left (-.) first rest

let calculate state =
  if Myargs.is_error state then
    Result.error (Myargs.get_error state)
  else
    Result.ok (state, calc state)

