(* calc.mli *)
(** [Calc] hanterar den önskade operationen, dvs addition eller
   subtraktion.  Men det görs bara om det inte skett ett tidigare fel.
   Om så är fallet, så skickas felmeddelandet vidare.  *)

(** [state] contains the state of the calculation *)
type state

(** [default_state] is the default sate of a calculation *)
val default_state: state
  
(** [is_error state] is [true] if there was some error *)
val is_error: state -> bool

(** [get_error state] returns the [string] of the error *)
val get_error: state -> string

(** [is_addition s] is [true] if we should add *)
val is_addition: state -> bool

(** [is_subtraction s] is [true] if we should subtract *)
val is_subtraction: state -> bool

(** [is_verbose s] is [true] if we should be verbose *)
val is_verbose: state -> bool
  
(** [get_value s] gives the result of calculation *)
val get_value: state -> float

(** [num_arguments s] give number of [float] arguments *)
val num_arguments: state -> int
  
(** [get_argyments s] give a [float list] a the arguments *)
val get_arguments: state -> float list

(** [parse argv] parse the [string array] into a [state] *)
val calculate: Myargs.state -> state

