(* myargs.mli *)
(** [Myargs] hanterar tolkning av kommandots argument samt om något
   går fel, returnerar ett fel.

    Programmet anropas enligt följande:

    my_pgm [-s|--subtrahera] tal1 tal2

    Programmet skall hantera fel antal argument och felaktiga
   switchar.  I tillståndet så skall det lagras de två talen samt om
   man vill subtrehera.
*)

(** [state] contains the state of the argument parsing *)
type state

(** [default_state] is the default state of the program *)
val default_state: state
  
(** [is_error state] is [true] if there was some error *)
val is_error: state -> bool

(** [get_error state] returns the [string] of the error *)
val get_error: state -> string

(** [is_addition s] is [true] if we should add *)
val is_addition: state -> bool

(** [is_subtraction s] is [true] if we should subtract *)
val is_subtraction: state -> bool

(** [is_verbose s] is [true] if we want to be verbose *)
val is_verbose: state -> bool

(** [num_arguments s] return number of arguments *)
val num_arguments: state -> int

(** [get_arguments s] returns [string list] of all arguments *)  
val get_arguments: state -> float list

(** [parse argv] parse the [string array] into a [state] *)
val parse: string array -> state

