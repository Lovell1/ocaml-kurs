(* myargs.ml *)
(** [Myargs] hanterar tolkning av kommandots argument samt om något
   går fel, returnerar ett fel.

    Programmet anropas enligt följande:

    my_pgm [-s|--subtrahera] tal1 tal2

    Programmet skall hantera fel antal argument och felaktiga
   switchar.  I tillståndet så skall det lagras de två talen samt om
   man vill subtrahera.
*)

(* [op] möjliga interna tillstånd för vald operation *)
type op =
  | Adding
  | Subtracting

      (* [my_state] interna tillståndet för tolkning av argumenten *)
type my_state =
  {
    verbose: bool ref;
    operation: op ref;
    vals: float array ref
  }

(* [state] handles state from parsing and errors *)
type state = (my_state, string) result

let default_my_state = {
  operation = ref Adding;
  vals = ref [| |];
  verbose = ref false
}
let default_state = Result.ok default_my_state
let get_my_state s = (Result.value s ~default:default_my_state)
                     
let is_error = Result.is_error

let get_error = Result.get_error

let is_addition s = Adding = !((get_my_state s).operation)

let is_subtraction s = Subtracting = !((get_my_state s).operation)

let is_verbose s = !((get_my_state s).verbose)
                        
let num_arguments s = Array.length !((get_my_state s).vals)

let get_arguments s = Array.to_list (Array.copy !((get_my_state s).vals))

(*
*)
let spec { operation; vals; verbose } =
  [("--subtraction", Arg.Unit (fun arg -> operation := Subtracting), " or" );
   ("-s", Arg.Unit (fun () -> operation := Subtracting), "\tsubtract arguments" );
   ("--addition", Arg.Unit (fun () -> operation := Adding), " or" );
   ("-a", Arg.Unit (fun () -> operation := Adding), "\tadd arguments" );
   ("--verbose", Arg.Set verbose, " or");
   ("-v", Arg.Set verbose, "\tbe verbose and print debug information");
   ("--", Arg.Rest (fun s -> vals := Array.append !vals [|try float_of_string s with Failure e -> raise (Arg.Bad e)|]),
    "\tfollowing arguments are used in the calculation")]
  
let parse argv =
  let dflt_state = default_state in
  let vals = (Result.get_ok default_state).vals in
  let add_args s = vals := 
      match float_of_string_opt s with
      | None -> raise (Arg.Bad "Try to use a non float number")
      | Some f -> Array.append !vals [| f |] in
  try
    Arg.parse_argv argv (spec (Result.get_ok default_state)) add_args
      "Add or subtract the arguments.\n\tcalc [-s|--subtraction|-a|--addition] [--] num1 num2\n";
    dflt_state
  with
    Arg.Help e -> Error e
  | Arg.Bad e -> Error ("E: "^e)
  
