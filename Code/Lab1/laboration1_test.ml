(* fil: laboration1-test.ml *)

(**
 * Program som testar en implementation av modulen [My_list]
 *
 * Datum      Namn               Beskrivning
 * ========== ================== ===================================
 * 2019-04-17 Anders Jackson     Rensa upp och anpassa till My_list
 *)

(** Modulen [Stdlib] har massa trevliga metoder, och alla metoder i
   [Stdlib] finns alltid.  ( [Stdlib] hette tidigare [Perversive])

   Den behöver ej öppnas/importeras med [open Stdlib]

   [val print_int: int -> unit]
   [val print_string: string -> unit]
   [val print_endline: string -> unit]
   [val print_newline: unit -> unit]
   [val string_of_int: int -> string]
   [val string_of_bool: bool -> string]
   [val List.iter ('a -> unit) -> 'a list -> ()]

   utför funktionen på alla element i listan 
 *)

(* Läs in modulen som skall testas *)
open My_list

let main () =
  (* funktion som utför [f] på varje [i] som accumuleras i [fold].
     [val string_of_f_fold: ('a -> string) -> 'a -> string]
   *)
    let string_of_f_fold f i acc =
      acc^" "^(f i) in
    (* Använder curry:ing på [string_of_f_fold] med [string_of_int]
       för att "ta bort" ett argument till [f] i [fold]. Sedan skrives
       resultat ut
     *)
    let print_acc_int_fold a lst =
      print_endline (fold (string_of_f_fold string_of_int) a lst) in
    (* Definierar några datamängder *)
    let data1 = empty in
    let data2 = cons 1 (cons 2 empty) in
    let data3 = empty |> cons 2 |> cons 1 in
    (* [data2 == cons (first data2) (rest data2)] *)
    let data4 = cons (first data2) (rest data2) in
    let data5 = append data2 data2 in
    let data6 = append data2 data4 in
    (* Testar funktioner på datamängderna *)
    print_newline (); print_endline "Test av 'a my_list"; print_newline ();
    (* Skriv ut datamängderna *)
    print_string (fold (string_of_f_fold string_of_int) "data1: " data1);
    print_newline ();
    print_endline (fold (string_of_f_fold string_of_int) "data2: " data2);
    print_endline (fold (string_of_f_fold
                           (fun a -> "string: "^(string_of_int a)))
                     "data3: "
                     data3);
    print_acc_int_fold "data4: " data4;
    print_acc_int_fold "data5: " data5;
    print_acc_int_fold "data6: " data6;
    (* testar [map] på [data3]. Där vi gör utför [string -> int ]-funktionen *)
    print_acc_int_fold "map data3: " (map (fun a -> a) data3);
    (* Testa [isempty] *)
    print_endline (string_of_bool (is_empty data1));
    print_endline (string_of_bool (is_empty data2));
    (* Testar [length] *)
    print_string ("length empty: "^(string_of_int (length empty)));
    print_newline ();
    print_string ("length data2: "^(string_of_int (length data2)));
    print_newline ();
    print_string ("length data3: "^(string_of_int (length data3)));
    print_newline ();
    print_acc_int_fold "from_list: " (from_list [1; 4; 2; 3]);
    List.iter print_int (as_list data1);
    print_newline ();
    List.iter print_int (as_list data2);
    print_newline ();
    (* Fånga felet och skriv ut meddelande för funktionsanropen [first
       empty], [rest empty], [last empty] *)
    (try
       print_int (first data1)
     with
       EmptyList msg -> print_string ("Error: "^msg)); print_newline ();
    (try print_acc_int_fold "rest(data1): " (rest data1)
     with EmptyList msg -> print_string ("Error: "^msg)); print_newline ();
    (try print_int (last data1)
     with EmptyList msg -> print_string ("Error: "^msg)); print_newline ()

(* Starta huvudprogrammet *)
let _ =
  main ()

(* eof *)
