(** 
    Modulen My_list

    Datatypen ['a my_list] är en egen implementation av ['a list].

    Ni skall skriva följande funktioner för datastrukturen.

    2021-04-15 Anders Jackson
*)

(** Define abstrakta datatypen ['a my_list] *)
type 'a my_list

(** Vid felaktiga värden så skall [raise EmptyList string] ske,
   exempelvis [raise (EmptyList "empty list in head")] vid [head empty].
*)
exception EmptyList of string

(** [empty] är en tom lista, konstant, som kan användas för att skapa
   [my_list] utan att veta hur datatypen exakt ser ut *)
val empty: 'a my_list

(** [first lst] ger första elementet i listan *)
val first: 'a my_list -> 'a
(** [last lst] ger sista elementet i listan *)
val last: 'a my_list -> 'a
(** [rest lst] ger en lista utan första elementet. Om listan är
   [empty], så kastas ett [EmtpyList "felmeddelande"] *)
val rest: 'a my_list -> 'a my_list
(** [cons elm rst] tar och skapar en ny lista där [elm] är första
   element och [rst] är resen av listan *)
val cons: 'a -> 'a my_list -> 'a my_list

(** [is_empty lst] ger [true] om listan är [empty] *)
val is_empty: 'a my_list -> bool
(** [length lst] ger antal element i listan *)
val length: 'a my_list -> int

(** [map f lst] ger en lista där varje element [el] i [lst] har bytts
   mot [f el] *)
val map: ('a -> 'b) -> 'a my_list -> 'b my_list
(** [fold f acc lst] ger ett värde där f utförs på varje [el] från
   [lst] och [acc] för att ge en ny [acc], som slutligen returneras *)
val fold: ('a -> 'b -> 'b) -> 'b -> 'a my_list -> 'b

(** [append lst1 lst2] slår ihop [lst1] med [lst2] i den ordningen *)
val append: 'a my_list -> 'a my_list -> 'a my_list

(** [as_list lst] konverterar ['a my_list] till ['a list] *)
val as_list: 'a my_list -> 'a list
(** [from_list lst] konvertera ['a list] till ['a my_list] *)
val from_list: 'a list -> 'a my_list

(* eof *)
