(** 
    Modulen My_list

    Datatypen ['a my_list] är en egen implementation av ['a list].

    Ni skall skriva följande funktioner för datastrukturen.

    2020-04-20 Anders Jackson
*)

(** Define abstrakta datatypen ['a my_list] *)
type 'a my_list = 
  | Empty
  | Cons of 'a * 'a my_list

(** Vid felaktiga värden så skall [raise EmptyList string] ske,
   exempelvis [raise (EmptyList "empty list")] vid [head empty].
*)
exception EmptyList of string

(** [empty] är en tom lista, konstant, som kan användas för att skapa
   [my_list] utan att veta hur datatypen exakt ser ut *)
let empty = Empty

(** [first lst] ger första elementet i listan *)
let first lst =
  match lst with
  | Empty -> raise (EmptyList "first på tom lista")
  | Cons (first,_rest) -> first

(** [last lst] ger sista elementet i listan *)
let rec last lst = raise (EmptyList "Not implemented")
(** [rest lst] ger en lista utan första elementet. Om listan är
   [empty], så kastas ett [EmtpyList "felmeddelande"] *)
let rest lst = raise (EmptyList "Not implemented")
(** [cons elm rst] tar och skapar en ny lista där [elm] är första
   elementy och [rst] är resen av listan *)
let cons elm rst = raise (EmptyList "Not implemented")
(** [is_empty lst] ger [true] om listan är [empty] *)
let is_empty lst = raise (EmptyList "Not implemented")
(** [lenth lst] ger antal element i listan *)
let rec length lst = raise (EmptyList "Not implemented")
(** [map f lst] ger en lista där varje element [el] i [lst] har bytts
   mot [f el] *)
let rec map f lst = raise (EmptyList "Not implemented")
(** [fold f acc lst] ger ett värde där f utförs på varje [el] från
   [lst] och [acc] för att ge en ny [acc], som slutligen returneras *)
let rec fold f acc lst = raise (EmptyList "Not implemented")
(** [append lst1 lst2] slår ihop [lst1] med [lst2] i den ordningen *)
let rec append lst1 lst2 = raise (EmptyList "Not implemented")
(** [as_list lst] konverterar ['a my_list] till ['a list] *)
let rec as_list lst = raise (EmptyList "Not implemented")
(** [from_list lst] konvertera ['a list] till ['a my_list] *)
let rec from_list lst = raise (EmptyList "Not implemented")

(* eof *)
