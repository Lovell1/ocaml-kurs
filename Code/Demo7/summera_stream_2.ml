(*
 * Programmet läser in flyttal från en fil
 * och skriver ut summan av alla talen.
 *
 * Börja med att läsa från en fil, ex "data.txt".
 * Skriv bara ut varje rad.
 *
 * 20190507 Anders Jackson Första versionen
 * 20190508 Anders Jackson Implementation med float Stream.t 
 *                          och felhantering om ej flyttal
 *
 * Kompilera:     C-c C-c  : "ocamlbuild -cflags -annot summera.byte"
 * Leta fel:      C-c C-x eller [f3] eller [S-f3]
 * Exekvera:      M-x shell: "./summera.byte"
 * Testa i REPL:  Tuareg -> Interactive mode -> Run OCaml REPL
 *)

let processa_raden tal acc = tal +. acc

let rec processa_rader stream acc =
  match Stream.peek stream with
  | None -> acc
  | Some _ ->
     (processa_raden (Stream.next stream) acc) |> processa_rader stream
(*   processa_rader stream (processa_raden (Stream.next stream) acc) *)

let main filnamn =
  let infil = open_in filnamn in
  let callback infil number = try
      Printf.printf "Rad %d\n" number;
      Some (float_of_string (input_line infil))
    with
    | End_of_file -> None
    | Failure str -> Some 0.0 in
  let stom = Stream.from (callback infil) in
  try
    print_float (processa_rader stom 0.); print_newline ();
    close_in infil
  with e ->
    close_in infil;
    raise e

let () =
  main "data.txt"

