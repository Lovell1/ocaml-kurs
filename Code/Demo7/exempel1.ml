(**
   Läs in data2.txt som består av rader med namn, skonummer och ålder.

   Det skall representeras som 
   type person = (string*int*int)
   eller
   type person = { namn:string; skonummer:int ; alder:int}

   Ni skall skapa en person Stream som programmet kan använda sig av för att skriv ut datat som 
   "Person: Anders
      Skonummer: 45
      Ålder: 56"
 *)

(* type person = (string*int*int) *)
type person = { namn:string; skonummer:int; alder:int }


let main filnamn =
  let infil = open_in filnamn in
  let callback infil _num : person option =
    try
      let raden = input_line infil |> String.trim in
      let falt = String.split_on_char ' ' raden in
      let fnamn = List.hd falt in
      let fskonummer = List.tl falt |> List.hd |> String.trim |> int_of_string in
      let falder = List.tl falt |> List.tl |> List.hd |> String.trim |> int_of_string in
      Some { namn=fnamn ; skonummer= fskonummer; alder = falder }
    with
    | End_of_file -> None in
  let person_stream = Stream.from (callback infil) in
  try
    Stream.iter (
        fun arg ->
        Printf.printf "Person: %s\n Skonummer: %d\n Ålder: %d\n" arg.namn arg.skonummer arg.alder
      )
      person_stream;
    close_in infil
  with e ->
    close_in infil;
    raise e

let () =
  main "data1.txt"
