(*
 * Detta är ett program som demonstrerar hur man kan skapa Set (mängder)
 * från Ocamls module Set.
 *
 * Här sätter jag att datat lagras via modul-funktorn Data, som
 * tar en modul som sätter vad datat skall lagras som. Den har 
 * datatypen t och funktionen compare som Set använder sig av för
 * att kolla dubletter.
 *
 * Den här implementationen använder fältet key för att jämföra datat
 * och har därför en egen compare som bara jämför fältet key.
 * 
 * Använder man standardmoduler, så jämför de all data, exempelvis med
 * Set.Make(String) etc.
 *
 * 20190528 Anders Jackson - Testprogram för standardmodulen Set.
 *)

(* En funktor som genererar en modul för att manipulera data *)
module Data(D: sig type elt end) = struct
  type key = int
  (* Det data som stoppas in i Set *)
  type t = { key:key; data:D.elt }
  let create key data = { key; data }
  let get_key { key } = key
  let get_data { data } = data
  (* testar två t med varandar, används av Set (här kollar vi bara nyckeln key) *)
  let compare {key=key1} {key=key2} = Pervasives.compare key1 key2
end

(* Skapar en module som hanterar string som data *)
module StringData = Data(struct type elt = string end)

(* Lagrar StringData.t (som är string) och int som nyckel *)
(* Set använder StringSet.t för att lagra data: elt samt funktionen
   StringSet.compart för jämförelse *)
module DataSet = Set.Make(StringData)

(* Provkör. Notera att Set returnerar en ny mängd när den ändrar *)
let () =
  (* val a : DataSet.elt -> DataSet.t -> DataSet.t *)
  let a = DataSet.add in
  let open StringData in
  let d = DataSet.empty
          |> a (create 1 "Hello")
          |> a (create 3 "Jackson")
          |> a (create 1 "Anders")
          |> a (create 3 "Anders")
          |> a (create 2 "Anders")
          |> a (create 4 "Kaka")
  in
  let print_string_data {key; data} =
    print_int key; print_endline (": "^data)
  in
  DataSet.iter print_string_data d;
  List.iter print_string_data (DataSet.elements d)

(* eof *)
