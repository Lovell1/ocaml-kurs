(*
 * Läs argument och växlar från kommandoraden.
 *
 * https://ocaml.org/learn/tutorials/command-line_arguments.html
 * http://caml.inria.fr/pub/docs/manual-ocaml/libref/Arg.html
 *
 * Här använder vi modulen Arg från standardbiblioteket.
 * 
 * Progammet specificerar några växlar, och tar och sedan 
 * tolkar och sätter några variabler utifrån vad som kommer på
 * kommandoraden. 
 *
 * ocamlbuild -cflags -annot get_switches.byte
 * 
 * $ ./get_switches.byte -a -b 13 --bara=12 -f fds -f=apa -- a b c
 * A:            true
 * B:            12
 * Filename:     apa
 * Rest of line: a b c 
 * $ -/get_switches.byte --help
 *
 * 20190527 Anders Jackson
 *)

(* Skapa variablerna som sätts enligt kommandoraden *)
let a = ref false
let b = ref 0
let filnamn = ref ""
let restofargs = ref ""

(* Tolka kommandoraden *)
let () =
  let spec = [("-a", Arg.Set a, "\tor");
              ("--and", Arg.Set a, "\tSet and to true");
              ("-n", Arg.Clear a, "\tSet and to false");
              ("-b", Arg.Set_int b, "\tor");
              ("--bara", Arg.Set_int b, "\tSet bara to the integer argument ");
              ("-f", Arg.Set_string filnamn, "\tor");
              ("--fil", Arg.Set_string filnamn, "\tSet file name to the argument");
              ("--", Arg.Rest (fun s -> restofargs := !restofargs ^ s ^ " "),
               "\tRest of arguments are stored");]
  in
  Arg.parse
    (Arg.align spec)
    (fun str -> filnamn := str)
    "My program does some great things.\nDon't know what it is yet.\n"
    
let () =
  (* Ok, så nu skriver vi ut argumenten. *)
  Printf.(printf "A:            %b\n" !a;
          printf "B:            %d\n" !b;
          printf "Filename:     %s\n" !filnamn;
          printf "Rest of line: %s\n" !restofargs)

(* eof *)
    
    
