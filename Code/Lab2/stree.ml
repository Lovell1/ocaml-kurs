(* fil: stree.ml *)

(** Här är en implementation av modulen [Stree] som är ett generellt
   Binärt sökträd som vi abstraherar ut jämförelseoperationen
   [compare] till en egen modul som vi skickar in i denna modul.

   Det gör att vi kan skapa binära sökträd som hanterar olika
   datatyper.  *)

(** [Stree.OrderedType] är det många moduler som är, exempelvis
   [String]-modulen i [Stdlib].  Den används för att jämföra [key] i
   [T]-trädet.  *)

module type OrderedType =
sig
  type t
  val compare: t -> t -> int
end

(** [Street.T] är det egentliga signaturen på binära sökträdet.  Men
   Modulkonstruktorn [Stree.Make] skapa en en modul för en viss typ av
   nycklar [key] utifrån den här signaturen. *)
module type T =
  sig
    (** [key] är datatypen på nycklarna som ['a t] sorteras efter *)
    type key
    (** ['a t] är datatypen på det som lagras i trädet *)
    type (+'a) t
    (** [empty] är ett tomt träd *)
    val empty: 'a t
    (** [is_empty tree] är [true] om [tree] är tomt *)
    val is_empty: 'a t -> bool
    (** [add (key,val) tree] returnerar ett träd som lagrat
       [(key,val)] i trädet [tree] *)
    val add: key*'a -> 'a t -> 'a t
    (** [remove key tree] ger ett nytt träd där [key] är borttaget.
       Om elementet inte existerar, så generera [invalid_argument
       string] exception *)
    val remove: key -> 'a t -> 'a t
    (** [remove_opt key tree] fungerar som [remove key tree], men ger
       [None] om [key] inte existerar. *)
    val remove_opt: key -> 'a t -> 'a t option
    (** [find key tree] ger elementet som lagrats med nyckeln [key] i
       [tree].  Om det inte existerar, generera [invalid_argument
       string] exception. *)
    val find: key -> 'a t -> 'a
    (** [find_opt key tree] fungerar som [find key tree] men
       returnerar [None] om nyckeln inte existerar *)
    val find_opt: key -> 'a t -> 'a option
    (** [cardinal tree] ger antalet element lagrat i [tree] *)
    val cardinal: 'a t -> int
    (** [exists fun tree] ger [true] om det existerar ett element i
       [tree] som för något element [e] där [true = fun e] *)
    val exists: ('a -> bool) -> 'a t -> bool
    (** [filter fun tree] ger ett träd där alla element i [tree] där
       [true = fun e] ör med *)
    val filter: ('a -> bool) -> 'a t -> 'a t
    (** [iter fun tree] traverserar genom alla element i träder och
       anropar [fun e] på elementen *)
    val iter: ('a -> unit) -> 'a t -> unit
    (** [fold fun tree acc] ger accumulatorn efter att traverserat
       genom [tree] och beräknat en ny [acc = fun e acc] för alla
       elementen *)
    val fold: ('a -> 'b -> 'b) -> 'a t -> 'b -> 'b
    (** [map fun tree] ger ett nytt träd som är isomorf med [tree],
       men alla elementen har ersatts med [fun e] *)
    val map: ('a -> 'b) -> 'a t -> 'b t
    (** [keys tree] ger en [key list] med alla nycklar [key] i
       trädet. Ordningen är obestämd *)
    val keys: 'a t -> key list
    (** [values tree] ger en [e list] med alla element [e] *)
  val values: 'a t -> 'a list
end

(** [Make] skapar en modul som ordnar ett träd enligt
   [OrderedType.compare]. Nycklarna [Stree.T.key] är av samma datatyp
   som [OrderedType.t].

   De data som kan lagras tillsammans med nyckeln [T.key] är ['a], dvs
   oberoende av [T.key]. Detta fungerar liknande Generics i Java.
   *)
module Make (Ord: OrderedType) : T with type key = Ord.t =
struct
  (* Här är implementationen *)
  type key = Ord.t
               
  type 'a t =
    | Empty
    | Node of 'a t * (key*'a) * 'a t

  let compare = Ord.compare

  let empty = Empty

  let is_empty tree =
    tree = Empty

  let rec add (k, v) = failwith "Not implemented"
  
  let rec remove_opt (k:key) tree = failwith "Not implemented"

  let remove k tree = failwith "Not implemented"
       
  let rec find_opt key tree = failwith "Not implemented"

  let find key (tree:'a t) : 'a = failwith "Not implemented"

  let rec cardinal tree = failwith "Not implemented"

  let rec exists pred tree = failwith "Not implemented"

  let rec filter pred tree = failwith "Not implemented"

  let rec iter f tree = failwith "Not implemented"

  let rec fold f tree acc = failwith "Not implemented"

  let rec map f tree = failwith "Not implemented"

  let rec keys = failwith "Not implemented"

  let rec values tree = failwith "Not implemented"

end
(* 
   Exempelkod
 *)
(*
let _ =
  let module StringStree = Make(String) in
  let open StringStree in 
  let s0 = empty in
  let s1 = add ("a", 1) s0 in
  let s2 = add ("ab", 2) s1 in
  let s3 = remove "a" s2 in
  print_endline "Stree - binary search tree";
  print_int (cardinal s3); print_newline ();
  print_int (List.length (keys s3)); print_newline ();
  print_int (List.length (values s3)); print_newline ()
 *)
(* eof *)
